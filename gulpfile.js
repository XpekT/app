var gulp = require('gulp'),
	minify = require('gulp-minify-css'),
	autoprefix = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	less = require('gulp-less'),
	livereload = require('gulp-livereload');

/* Directories */
var inputLess = 'assets/less',
	outputLess = 'public/stylesheets';

var inputJs = 'assets/js',
	outputJs = 'public/javascripts';

/* Less Task */
gulp.task('less', function() {

	return gulp.src(inputLess + '/**/*.less')
			   .pipe(less())
			   .pipe(minify({ keepBreaks: false }))
			   .pipe(autoprefix('last 2 versions'))
			   .pipe(livereload())
			   .pipe(gulp.dest(outputLess));


});

/* Js Task */
gulp.task('js', function() {

	return gulp.src(inputJs + '/**/*.js')
			   .pipe(uglify())
			   .pipe(livereload())
			   .pipe(gulp.dest(outputJs));

});

/* Watch Task */
gulp.task('watch', function() {

	livereload.listen();
	gulp.watch(['views/**/*']).on('change', livereload.changed);
	gulp.watch(inputLess + '/**/*', ['less']);
	gulp.watch(inputJs + '/**/*', ['js']);

});

/* Default Task */
gulp.task('default', ['less', 'js', 'watch']);