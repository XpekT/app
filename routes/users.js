var express = require('express');
var router = express.Router();
var form = require('express-form'),
	filter = form.filter,
    validate = form.validate;
var Cryptr = require("cryptr"),
    cryptr = new Cryptr('7hdasmon2378|nmasoç!çnasn7vdsvs89dy7dc8gbsd');
var bcrypt = require('bcrypt');
var usersModel = require('../models/users');
var moment = require('moment');

/* GET - Register */
router.get('/register', function(req, res, next) {

	if(!req.signedCookies.appsession) {
		res.render('register', {
			title: 'App - Register',
			csrfToken: req.csrfToken()
		});
	}
	else {
		res.redirect("/");
	}

});

/* POST - Register */
router.post('/register', 

	form(
		filter("email").trim(),
	    validate("email").required().isEmail(),
	    filter("password").trim(),
	    validate("password").required(),
	    validate("re_password").equals('field::password')
	), 

	function(req, res, next) {

		if(!req.form.isValid){

			res.send({
				errors: req.form.errors
			});

		}
		else {

			/* Check if email already exists */
			usersModel.findOne({ email: req.form.email }, function(err, user){

				if(err) {
					res.send({
						errors: [err]
					});
				}
				else {

					if(user) {

						res.send({
							errors: [req.form.email + " already exists!"]
						});

					}
					else {

						/* Encrypt Password */
						bcrypt.hash(req.form.password, 8, function(err, hash) {

							if (err) {

								res.send({
									errors: [err]
								});

							}
							else {

								/* Create a new user */
								var user = new usersModel({
									email: req.form.email,
									password: hash,
									created: Date.now()
								});

								user.save(function(err) {

									if(err) {

										res.send({
											errors: [err]
										});

									}
									else {

										res.send({
											success: true,
											message: "Account Created Succesfully!"
										});

									}

								});

							}

						});

					}

				}

			});

		}

});

/* POST - Login */
router.post('/login',

	form(
		filter("email").trim(),
	    validate("email").required().isEmail(),
	    filter("password").trim(),
	    validate("password").required()
	), 

	function(req, res, next) {

		if(!req.form.isValid) {

			res.send({
				errors: req.form.errors
			})
		
		}
		else {

			/* Check if user exists */
			usersModel.findOne({ email: req.form.email }, function(err, user) {

				if(err) {

					res.send({
						errors: [err]
					});

				}
				else {

					if(!user) {

						res.send({
							errors: ["The email - " + req.form.email + " does not exist!"]
						});

					}
					else {

						/* If user exists, check password */
						bcrypt.compare(req.form.password, user.password, function(err, same) {

							if(err) {

								res.send({
									errors: [err]
								});

							}
							else {

								if(!same) {

									res.send({
										errors: ["Bad Credentials!"]
									});

								}
								else {

									res.cookie('appsession', cryptr.encrypt(user.email), { signed: true });

									res.send({
										success: true
									});

								}

							}

						});

					}

				}

			});
			

		}

});

/* GET Logout */
router.get('/logout', function(req, res, next) {

	if(req.xhr) {

		res.clearCookie('appsession');

		res.send({
			success: true
		});

	}
	else {

		res.status(404).redirect('404');

	}
	
});

/* GET - User's Profile */
router.get('/profile/:email', function(req, res, next) {

	/* Check if user exists */
	usersModel.findOne({ email: req.params.email }, 'email created', function(err, user) {

		if(err) {

			res.status(404).send(err);

		}
		else if(!user) {

			res.render('profile', {
				title: 'App - Profile',
				errors: ['The user ' + req.params.email + ' does not exist!']
			});

		}
		else {

			if(req.signedCookies.appsession) {

				var decrypted = cryptr.decrypt(req.signedCookies.appsession);

				usersModel.findOne({ email: decrypted }, 'email created', function(err, user) {

					if(err) {

						res.status(404).send(err);

					}
					else {

						res.render('profile', {
							title: "App - Profile",
							loggedUser: user,
							created: moment(user.created).fromNow()
						});

					}

				});

			}
			else {

	
				if(err) {

					res.status(404).send(err);

				}
				else {

					res.render('profile', {
						title: "App - Profile",
						user: user.email,
						created: moment(user.created).fromNow()
					});

				}

			}

		}

	});

});

module.exports = router;
