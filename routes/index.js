var express = require('express');
var router = express.Router();
var Cryptr = require("cryptr"),
    cryptr = new Cryptr('7hdasmon2378|nmasoç!çnasn7vdsvs89dy7dc8gbsd');
var userModel = require('../models/users');

/* GET home page. */
router.get('/', function(req, res, next) {

	if(req.signedCookies.appsession) {

		var decrypted = cryptr.decrypt(req.signedCookies.appsession)

		userModel.findOne({ email: decrypted }, 'email created', function(err, user) {

			if(err) {

				res.status(404).send(err);

			}
			else {

				res.render('index', {
					title: 'Express',
					loggedUser: user
				});

			}

		});

	}
	else {

		res.render('index', {
			title: 'Express',
			csrfToken: req.csrfToken()
		});

	}

});

module.exports = router;
