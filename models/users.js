var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var usersSchema = new Schema({
	email: { type: String, unique: true },
	password: String,
	created: Date
});

module.exports = mongoose.model('users', usersSchema);