$(document).ready(function() {

	$('.ink-dismiss').click(function() {

		$('.ink-alert').children('p').remove();

		$(".ink-alert").fadeOut('fast');

	});

	$(".login-li").click(function(e) {

		$('#login-form').fadeToggle('fast');

		e.preventDefault();

	});

	/* Login Form AJAX */
	$("#login-form").on('submit', function() {

		$('.login-errors').children('p').remove();

		var url = $(this).attr('action'),
			method = $(this).attr('method'),
			content = {};

		$(this).find('[name]').each(function(index, value) {

			var name = $(this).attr('name'),
				value = $(this).val();
				content[name] = value;

		});	

		$.ajax({
			url: url,
			type: method,
			data: content,
			success: function(data) {

				if(data.errors) {

					data.errors.forEach(function(error) {

						$('.login-errors').append('<p>&bullet; ' + error + '</p>')

					});

					$('.login-errors').fadeIn('fast');

				}
				else {

					$(".login-errors").fadeOut('fast');

					if(data.success === true) {
						
						location.href = "/";
					
					}
					else {
						
						location.href = "404";
					
					}

				}

			},
			error: function() {

				console.log("Error!");
			
			},
			statusCode: {
				403: function() {
					location.href = "/";
				}
			}
		});

		return false;

	});

	/* Register Form AJAX */
	$("#register-form").on('submit', function() {

		$(".registration-errors").children('p').remove();
		$(".registration-success").children('p').remove();

		var url = $(this).attr('action'),
			method = $(this).attr('method'),
			content = {};

		$(this).find('[name]').each(function(index, value) {

			var name = $(this).attr('name'),
				value = $(this).val();
				content[name] = value;

		});	

		$.ajax({
			url: url,
			type: method,
			data: content,
			success: function(data) {

				if(data.errors) {

					data.errors.forEach(function(error) {

						$(".registration-errors").append('<p>&bullet; ' + error + '</p>');

						$(".registration-errors").fadeIn('fast');

					});

				}
				else {

					$(".registration-errors").fadeOut('fast');

					if(data.success === true) {

						$(".registration-success").append('<p>' + data.message + '</p>');

						$(".reset").click();

					}

				}

			},
			error: function() {
				console.log('Error!');
			},
			statusCode: {
				403: function() {
					location.href = "/";
				}
			}
		});

		return false;

	});

	/* Logout AJAX */
	$(".logout-btn").click(function() {
		
		$.ajax({
			url: '/users/logout',
			type: 'GET',
			success: function(data) {

				if(data.success === true) {
					location.href = '/';
				}

			}
		});

	});

});